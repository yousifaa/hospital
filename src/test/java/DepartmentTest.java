import hospital.Department;
import hospital.Employee;
import hospital.Patient;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    Department departmentTest;

    @BeforeEach
    void createTestData() {
        this.departmentTest = new Department("emt");

        departmentTest.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        departmentTest.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        departmentTest.getEmployees().add(new Employee("Rigmor", "Mortis", ""));

        departmentTest.getPatients().add(new Patient("Inga", "Lykke", ""));
        departmentTest.getPatients().add(new Patient("Ulrik", "Smål", ""));
    }

    @Test
    @DisplayName("Test method that should have positive result")
    void positiveRemoveTest() {

        try{
            departmentTest.remove(departmentTest.getEmployees().get(0));
        }
        catch (RemoveException r){
            System.out.println(r.getMessage());
        }

        assertEquals(2,departmentTest.getEmployees().size());
    }

    @Test
    @DisplayName("Test method with objects not in employee or patient list")
    void notInListRemoveTest() {
        Patient p = new Patient("Bob","Ross","123");
        Employee e = new Employee("Dr.","Stone","123");

        try {
            departmentTest.remove(p);
        }
        catch (RemoveException r){
            System.out.println(r.getMessage());
        }

        try {
            departmentTest.remove(e);
        }
        catch (RemoveException r){
            System.out.println(r.getMessage());
        }

        assertEquals(2,departmentTest.getPatients().size());
        assertEquals(3,departmentTest.getEmployees().size());
    }

    @Test
    @DisplayName("Test with null object")
    void voidObjectRemoveTest() {
        try {
            departmentTest.remove(null);
        }
        catch (RemoveException r) {
            System.out.println(r.getMessage());
        }
    }

}