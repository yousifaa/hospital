package hospital.exception;

import java.io.Serializable;

/**
 * The type Remove exception.
 */
public class RemoveException extends Exception implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Remove exception.
     *
     * @param message the error message that is given in the remove method.
     */
    public RemoveException(String message) {
        super(message);
    }

}
