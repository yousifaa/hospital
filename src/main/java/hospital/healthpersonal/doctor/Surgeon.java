package hospital.healthpersonal.doctor;

import hospital.Patient;

/**
 * The type Surgeon.
 */
public class Surgeon extends Doctor {
    /**
     * Instantiates a new Surgeon.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
