package hospital;

import hospital.exception.RemoveException;

/**
 * The type Hospital client.
 */
public class HospitalClient {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Hospital hospital = new Hospital("Gjøvik Sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);
        Patient p = new Patient("Rodger", "Robinson" , "05346");
        try{
            hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));
        }catch (RemoveException r){
            System.out.println(r.getMessage());
        }

        try{
            hospital.getDepartments().get(0).remove(p);
        }catch (RemoveException r){
            System.out.println(r.getMessage());
        }
    }
}
