package hospital;

import java.util.ArrayList;

/**
 * The type hospital.
 */
public class Hospital {
    private final String hospitalName;
    private ArrayList<Department> departmentsList;

    /**
     * Instantiates a new hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departmentsList = new ArrayList<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Set hospital name.
     */
    public void setHospitalName(){

    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public ArrayList<Department> getDepartments() {
        return departmentsList;
    }

    /**
     * Sets departments.
     *
     * @param departmentsList the departments list
     */
    public void setDepartments(ArrayList<Department> departmentsList) {
        this.departmentsList = departmentsList;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentsList=" + departmentsList.toString() +
                '}';
    }

}
