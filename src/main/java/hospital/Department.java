package hospital;

import java.util.ArrayList;
import java.util.Objects;
import hospital.exception.RemoveException;

/**
 * The type hospital.Department.
 */
public class Department {
    private String departmentName;
    private ArrayList<Patient> patientsList;
    private ArrayList<Employee> employeesList;

    /**
     * The constructor for the department class. Initializes two ArrayLists for patients and employees.
     *
     * @param departmentName the name of the department.
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.patientsList = new ArrayList<>();
        this.employeesList = new ArrayList<>();
    }

    /**
     * Getter for the department name.
     *
     * @return department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets employees ArrayList.
     *
     * @return the employees
     */
    public ArrayList<Employee> getEmployees() {
        return employeesList;
    }

    /**
     * Gets patients ArrayList.
     *
     * @return the patients
     */
    public ArrayList<Patient> getPatients() {
        return patientsList;
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee) {
        employeesList.add(employee);
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient){
        patientsList.add(patient);
    }

    /**
     * A method to remove patients or employees from their respective ArrayLists.
     *
     * @param o the object of class Person that is given.
     * @throws RemoveException the exception thrown when an error is expected.
     */
    public void remove(Person o) throws RemoveException {
        if (Objects.isNull(o)) {
            throw new RemoveException("Null object provided");
        }

        else if (o instanceof Patient) {
            if (patientsList.size()==0){
                throw new RemoveException("Patient list is empty");
            }
            else if (!patientsList.contains(o)) {
                throw new RemoveException(("Patient is not a part of the patient list"));
            }
            else {
                patientsList.remove(o);
                System.out.println("Patient removed successfully");
            }
        }

        else if (o instanceof Employee) {
            if (employeesList.size()==0){
                throw new RemoveException("Employee list is empty");
            }
            else if(!employeesList.contains(o)){
                throw new RemoveException(("Employee is not a part of the employee list"));
            }
            else{
                employeesList.remove(o);
                System.out.println("Employee removed successfully");
            }
        }

        else {
            throw new RemoveException("Person not found in patient or employee list");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(patientsList, that.patientsList) &&
                Objects.equals(employeesList, that.employeesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, patientsList, employeesList);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", patientsList=" + patientsList.toString() +
                ", employeesList=" + employeesList.toString() +
                '}';
    }
}
